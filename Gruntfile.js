module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
/*
    copy: {
      main: {
        files: [
          {
            flatten:true,
            expand: true,
            src: ['bower_components/font-awesome/fonts/*'],
            dest: 'public/fonts/'
          },
        ],
      },
    },
*/
	browserify: {
		dist: {
			files: {
			  'public/js/index.js': ['public/js/pages/index.js'],
			  'public/js/login.js': ['public/js/pages/login.js']

			},
			options: {
			  transform: [['babelify', {presets: ['react', 'es2015']}]]
			}
		}
	},

    uglify: {
      options: {
        banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
      },
      build: {
        dest: 'public/js/site.min.js',
        src: [
          'public/js/site.js'
        ]
      }
    },
/*
    sass: {
			dist: {
				files: {
					'public/css/src/styles.css' : 'sass/styles.scss'
				}
			}
		},

    cssmin: {
      options: {
        shorthandCompacting: false,
        roundingPrecision: -1
      },
      target: {
        files: {
          'public/css/<%= pkg.name %>.min.css': [
            'bower_components/font-awesome/css/font-awesome.min.css',
            'public/css/src/*.css'
          ]
        }
      }
    },
*/
	watch: {
		js: {
			files: ['public/js/components/*.js', 'public/js/pages/*.js'],
			tasks: ['browserify']
		}
		/*,
		css: {
			files: 'sass/*.scss',
			tasks: ['sass', 'cssmin']
		}*/
	}

  });

  // Copy
  //grunt.loadNpmTasks('grunt-contrib-copy');

  // Javascript
  grunt.loadNpmTasks('grunt-browserify');
  grunt.loadNpmTasks('grunt-contrib-uglify');

  // CSS
  //grunt.loadNpmTasks('grunt-contrib-sass');
  //grunt.loadNpmTasks('grunt-contrib-cssmin');

  // Watch
  grunt.loadNpmTasks('grunt-contrib-watch');

  // Default task(s).
  grunt.registerTask('default', [/*'copy',*/ 'browserify', /*'sass', 'cssmin',*/ 'watch']);

};
