# Material CMS #

### Overview ###

Material CMS is a headless content management system built in React.js and following Google's Material Design UI language. It makes use of Mongoose schemas and can be easily added to any preexisting application that uses Mongoose.

### Setup ###

Just add the following to your Node.js server to get up and running:
```
var cms = require('material-cms');

cms.init(
  'my-mongo-url.com/my-database',
  {
    MySchema1: MySchema1,
    MySchema2: MySchema2,
    MySchema3: MySchema3
  }
);
```

The first argument is the database URL. Note that this is not the full mongo string; your credentials will be asked for when you visit the CMS in your browser. The second argument should contain all of your mongoose schemas, keyed by their names. Optionally, you can pass a third argument to manually specify the port number. Material CMS will run in the same process as your main app, but on a separate port. By default it binds to `8001`.

**NOTE: You may experience a breaking error stemming from the Mongoose utils. I've filed a bug report with Mongoose, along with a proposed single-line change that seems to fix it as far as I can tell. It should be easy to add the line to Mongoose yourself to get your local version working. You can follow the status and see the solution here: https://github.com/Automattic/mongoose/issues/4152**

### Current State and Future Plans ###

Material CMS is currently in a pretty early state. Basic CRUD operations work well enough, as does sorting, but that's about it. I would not recommend using this in a production environment yet.

The following additions are on my radar and I'll probably continue to work on them in my spare time:

 - Support for mongoose property constraints (required, regex, etc.)
 - Support for displaying/selecting relations to other documents
 - Search
 - Some kind of solution for handling large text editing areas, preferably without adding a layer on top of Mongoose
 - General stability improvements/quieting of the tempest of errors that currently is the browser console

Stretch ideas that may or may not ever happen:

 - In-UI schema editing
 - Storage of schemas in the database
 - Ability to run as a standalone process (pretty much dependent on the previous item)