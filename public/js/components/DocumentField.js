
import React from 'react';
import ReactDOM from 'react-dom';

import SelectField from 'material-ui/lib/select-field';
import MenuItem from 'material-ui/lib/menus/menu-item';
import TextField from 'material-ui/lib/text-field';
import DatePicker from 'material-ui/lib/date-picker/date-picker';
import Checkbox from 'material-ui/lib/checkbox';
//import FontIcon from 'material-ui/lib/font-icon';

function camelToSpace( string ) {
	var spaced = string.replace( /([A-Z])/g, " $1" );
	return spaced.charAt(0).toUpperCase() + spaced.slice(1);
}

var DocumentField = React.createClass({

	getInitialState: function() {

		return {
			editingSchema: false
		};

		return initialState;
	},

	_propagateTypeChange: function( event, key, payload ) {
		this.props.updateType(this.props.fieldIndex, payload);
	},

	_propagateNameChange: function( event, key, payload ) {
		this.props.updateName(this.props.fieldIndex, event.target.value);
	},

	_propagateValueChange: function( event, newVal ) {
		if(event === null) { // is DatePicker
			this.props.updateValue(this.props.fieldIndex, newVal);
		} else if(typeof(newVal) === 'boolean') { // is Checkbox
			this.props.updateValue(this.props.fieldIndex, newVal);
		} else {
			this.props.updateValue(this.props.fieldIndex, event.target.value);
		}
	},

	render: function() {
		return (
			<div className="document-field">

				{/* Name */}
				{(function(element){
					if(element.props.editingSchema) {
						return (
							<TextField
									className="document-field-name"
									type="text"
									value={element.props.name}
									style={{
										display: 'inline-block',
										width: '150px',
										textAlign: 'left'
									}}
									onChange={element._propagateNameChange} >
							</TextField>
						);
					} else {
						return (
							<div
									className="document-field-name"
									style={{
										display: 'inline-block',
										width: '150px',
										textAlign: 'left'
									}}>
								{camelToSpace(element.props.name)}
							</div>
						);
					}
				})(this)}

				{/* Value */}
				{(function(element){
					if(element.props.type === 'String') {
						return (
							<TextField
								className="document-field-value"
								value={element.props.value}
								type="text"
								style={{
									display: 'inline-block',
									width: '200px',
									marginLeft: '30px'
								}}
								onChange={element._propagateValueChange} />
						);
					} else if(element.props.type === 'Number') {
						return (
							<TextField
								className="document-field-value"
								value={element.props.value}
								type="number"
								style={{
									display: 'inline-block',
									width: '200px',
									marginLeft: '30px'
								}}
								onChange={element._propagateValueChange} />
						);
					} else if(element.props.type === 'Date') {
						return (
							<DatePicker
								className="document-field-value"
								value={element.props.value}
								style={{
									display: 'inline-block',
									width: '200px',
									marginLeft: '30px',
									lineHeight: '32px'
								}}
								textFieldStyle={{
									display: 'inline-block',
									width: '200px'
								}}
								onChange={element._propagateValueChange} />
						);
					} else if(element.props.type === 'Boolean') {
						return (
							<Checkbox
								className="document-field-value"
								checked={element.props.value}
								style={{
									display: 'inline-block',
									width: '200px',
									marginLeft: '30px'
								}}
								onCheck={element._propagateValueChange} />
						);
					} else {
						return (
							<span style={{
								display: 'inline-block',
								width: '200px',
								marginLeft: '30px',
								lineHeight: 'normal'
							}}>
								(Unsupported type '{element.props.type}')
							</span>
						);
					}
				})(this)}
			</div>
		)
	}

});

module.exports = DocumentField;
