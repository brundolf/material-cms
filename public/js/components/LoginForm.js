
import React from 'react';
import ReactDOM from 'react-dom';

import Paper from 'material-ui/lib/paper';
import TextField from 'material-ui/lib/text-field';
import FontIcon from 'material-ui/lib/font-icon';
import RaisedButton from 'material-ui/lib/raised-button';
import CircularProgress from 'material-ui/lib/circular-progress';

var LoginForm = React.createClass({

	getInitialState: function() {
		return {
			username: '',
			password: '',

			attemptingLogin: false,
			authenticationFailure: false
		};
	},

	componentDidMount: function() {
		this.refs['usernameField'].focus();
	},

	_propagateUsernameChange: function( event, newValue ) {
		this.setState({
			username: newValue,
			authenticationFailure: false
		})
	},
	_propagatePasswordChange: function( event, newValue ) {
		this.setState({
			password: newValue,
			authenticationFailure: false
		})
	},

	_handleKeyPress: function(e) {
	    if (e.key === 'Enter') {
	    	this._login();
	    }
	},
	_login: function() {
		var element = this;
		this.setState({
			attemptingLogin: true
		})

		// Send request
		fetch('/login', {
			method: 'POST',
			credentials: 'same-origin',
			headers: new Headers({
				"Content-Type": "application/json"
			}),
			body: JSON.stringify({
				username: this.state.username,
				password: this.state.password
			})
		}).then(function(response) {
			element.setState({
				attemptingLogin: false
			})

			response.json().then(function( json ){
				if(json.message === 'success') {
					window.location = '/';
				} else {
					element.setState({
						authenticationFailure: true
					})
				}
			});

		});
	},

	render: function() {
		return (
			<Paper zDepth={4} style={{ padding: '40px', width: '100%', height: '100%' }} onKeyPress={this._handleKeyPress}>
				<div className="document-title" style={{ fontSize: '36px', textAlign: 'center' }}>
					Material CMS Login
				</div>

				<div style={{ textAlign:'center', marginTop: '20px' }}>
					<TextField floatingLabelText="Username" style={{ display: 'block', minWidth: '80%', margin: '0 auto' }} onChange={this._propagateUsernameChange} ref="usernameField" errorText={( this.state.authenticationFailure ? ' ' : undefined)} />
					<TextField floatingLabelText="Password" style={{ display: 'block', minWidth: '80%', margin: '0 auto' }} onChange={this._propagatePasswordChange} ref="passwordField" errorText={( this.state.authenticationFailure ? 'Authentication Failed' : undefined)} type="password" />
				</div>

		  		<div style={{ textAlign: 'right', marginTop: '20px', lineHeight: '52px' }}>
					<CircularProgress size={0.5} style={{ verticalAlign: 'middle', display: (this.state.attemptingLogin ? 'inline-block' : 'none') }} />
					<RaisedButton label="Login" primary={true} onClick={this._login} style={{ verticalAlign: 'middle', marginRight: '10%', display: 'inline-block', lineHeight: '30px' }} />
				</div>
			</Paper>
		)
	}

});

module.exports = LoginForm;
