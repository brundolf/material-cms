
import React from 'react';
import ReactDOM from 'react-dom';

import Paper from 'material-ui/lib/paper';
import IconButton from 'material-ui/lib/icon-button';
import FontIcon from 'material-ui/lib/font-icon';
import RaisedButton from 'material-ui/lib/raised-button';
import Dialog from 'material-ui/lib/dialog';
import CircularProgress from 'material-ui/lib/circular-progress';


import DocumentField from './DocumentField.js';

var Document = React.createClass({

	getInitialState: function() {
		return {
			editingSchema: false,
			populated: false,

			modelName: this.props.modelName,
			objectId: this.props.objectId,
			fields: []
		};
	},

	componentWillMount: function() {
		if(this.props.open) {
			this._readSchemaAndValues();
		}
	},

	componentWillReceiveProps: function (nextProps) {
		if(nextProps.open) {
			this._readSchemaAndValues(nextProps);
		}
	},

	_readSchemaAndValues: function(nextProps) {
		var props = this.props
		if(typeof nextProps !== 'undefined') {
			props = nextProps;
		}

		var element = this;
		fetch('/api/' + props.modelName, {
			method: 'GET',
			credentials: 'same-origin',
			headers: new Headers({
				"Content-Type": "application/json"
			})
		}).then(function(dbSchema) {
			dbSchema.json().then(function(dbSchemaJson){
				fetch('/api/' + props.modelName + '/' + props.objectId, {
					method: 'GET',
					credentials: 'same-origin',
					headers: new Headers({
						"Content-Type": "application/json"
					})
				}).then(function(dbValues) {
					dbValues.json().then(function(dbValuesJson){
						var fields = [];

						for(var key in dbSchemaJson.paths) {
							if(key !== '_id' && key !== '__v') {
								fields.push({
									type: dbSchemaJson.paths[key].instance,
									name: dbSchemaJson.paths[key].path,
									value: dbValuesJson[key]
								})
							}
						}

						element.setState({
							populated: true,
							fields: fields
						});
					})
				});

				var fields = [];
				element.setState({ fields: fields });
			})
		});
	},

	_readSchema: function(nextProps) {
		var props = this.props
		if(typeof nextProps !== 'undefined') {
			props = nextProps;
		}

		var element = this;
		fetch('/api/' + props.modelName, {
			method: 'GET',
			credentials: 'same-origin',
			headers: new Headers({
				"Content-Type": "application/json"
			})
		}).then(function(response) {
			response.json().then(function(json){
				var fields = [];
				for(var key in json.paths) {
					if(key !== '_id' && key !== '__v') {
						fields.push({
							type: json.paths[key].instance,
							name: json.paths[key].path,
							value: undefined
						})
					}
				}
				element.setState({ fields: fields });
			})
		});
	},

	_readValues: function(nextProps) {
		var props = this.props
		if(typeof nextProps !== 'undefined') {
			props = nextProps;
		}

		var element = this;
		fetch('/api/' + props.modelName + '/' + props.objectId, {
			method: 'GET',
			credentials: 'same-origin',
			headers: new Headers({
				"Content-Type": "application/json"
			})
		}).then(function(response) {
			response.json().then(function(json){
				var fields = element.state.fields;
				for(var i = 0; i < fields.length; i++) {
					fields[i].value = json[fields[i].name];
				}
				element.setState({
					populated: true,
					fields: fields
				});
			})
		});
	},

	// Persistence
	_saveSchema: function() {
		// write to code

		/*
		Build object from fields:
		schema: {
			name: String,
			count: Number,
			birthday: Date,
			activated: Boolean
		}*/
	},

	_saveValues: function() {
		var element = this;

		// Build object from fields
		var values = {};
		for(var i = 0; i < this.state.fields.length; i++) {
			var field = this.state.fields[i];
			values[field.name] = field.value;
		}

		// Send request
		fetch('/api/' + this.props.modelName + '/' + this.props.objectId, {
			method: 'PUT',
			credentials: 'same-origin',
			headers: new Headers({
				"Content-Type": "application/json"
			}),
			body: JSON.stringify(values)
		}).then(function(response) {
			element._closeDocument();
		});
	},

	// Internal
	_toggleLock: function() {

		this.setState({
			editingSchema: !this.state.editingSchema
		});
		// save schema here?
	},
	_getLockIconLigature: function() {
		if(this.state.editingSchema) {
			return 'lock_open';
		} else {
			return 'lock_outline';
		}
	},
	_updateFieldType: function( fieldIndex, newType ) {
		var state = this.state;
		state.fields[fieldIndex].type = newType;
		state.fields[fieldIndex].value = undefined;
		this.setState(state);
	},
	_updateFieldName: function( fieldIndex, newName ) {
		var state = this.state;
		state.fields[fieldIndex].name = newName;
		this.setState(state);
	},
	_updateFieldValue: function( fieldIndex, newValue ) {
		var state = this.state;
		state.fields[fieldIndex].value = newValue;
		this.setState(state);
	},
	_openDocument: function() {

	},
	_closeDocument: function() {
		this.props.closeDocument();
		this.setState({
			populated: false
		});
	},

	_renderDocumentField: function( fieldIndex ) {
		var field = this.state.fields[fieldIndex];
		return (
			<DocumentField
				key={'document-field-' + fieldIndex}
				editingSchema={this.state.editingSchema}
				fieldIndex={fieldIndex}
				type={field.type}
				name={field.name}
				value={field.value}
				updateType={this._updateFieldType}
				updateName={this._updateFieldName}
				updateValue={this._updateFieldValue} />
		)
	},

	render: function() {
		return (
			<Dialog className="document" zDepth={4} open={this.props.open} bodyStyle={{ padding: '50px 100px' }}>
				<div className="document-title">
					{this.props.title}
				</div>

				<div style={{ textAlign:'center', minHeight: '120px' }}>
				{function(element){
					if(element.state.populated) {
						return Object
			            	.keys(element.state.fields)
			            	.map(element._renderDocumentField)
					} else {
						return (
							<CircularProgress />
						)
					}
				}(this)}
				</div>

		  		<div className="document-footer">
					<RaisedButton label="Save" primary={true} onClick={this._saveValues} style={{ marginRight: '20px' }} />
					<RaisedButton label="Close" onClick={this._closeDocument} />
				</div>
			</Dialog>
		)
	}

});

module.exports = Document;
