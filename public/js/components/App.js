
import React from 'react';
import ReactDOM from 'react-dom';

import AppBar from 'material-ui/lib/app-bar';
import LeftNav from 'material-ui/lib/left-nav';
import SelectableContainerEnhance from 'material-ui/lib/hoc/selectable-enhance';
let SelectableList = SelectableContainerEnhance(List);
import List from 'material-ui/lib/lists/list';
import ListItem from 'material-ui/lib/lists/list-item';
import TextField from 'material-ui/lib/TextField';
import FontIcon from 'material-ui/lib/font-icon';
import IconButton from 'material-ui/lib/icon-button';
import FlatButton from 'material-ui/lib/flat-button';
import getMuiTheme from 'material-ui/lib/styles/getMuiTheme';
import FloatingActionButton from 'material-ui/lib/floating-action-button';

import NewDocumentMenu from './NewDocumentMenu.js';
import Document from './Document.js';
import DocumentCollection from './DocumentCollection.js';


var injectTapEventPlugin = require("react-tap-event-plugin");
injectTapEventPlugin();

function camelToSpace( string ) {
	var spaced = string.replace( /([A-Z])/g, " $1" );
	return spaced.charAt(0).toUpperCase() + spaced.slice(1);
}

var App = React.createClass({

	getInitialState: function() {
		// load models
		return {
			muiTheme: getMuiTheme(),
			collections: [],
			selectedCollectionIndex: '0',
			currentSchema: [],
			sortField: '',
			searchString: '',

			documentOpen: false,
			currentDocument: {}
		};
	},

	componentWillMount: function() {
		this._loadCollections();
	},

	_loadCollections: function() {
		var element = this;
		fetch('/api/collections', {
			method: 'GET',
			credentials: 'same-origin',
			headers: new Headers({
				"Content-Type": "application/json"
			})
		}).then(function(response) {
			response.json().then(function(json){
				element.setState({
          collections: json
        });
        element._collectionChanged(undefined, '0');
			})
		});
	},

  _createNewCurrentDocument: function() {
    this._createNewDocument(this.state.collections[this.state.selectedCollectionIndex]);
  },

	_createNewDocument: function( modelName ) {
		this._closeCurrentDocument();

		var element = this;
		fetch('/api/' + modelName, {
			method: 'POST',
			credentials: 'same-origin',
			headers: new Headers({
				"Content-Type": "application/json"
			})
		}).then(function(response) {
			response.json().then(function( json ){
				element.setState({
					documentOpen: true,
					currentDocument: {
						modelName: modelName,
						objectId: json._id
					}
				});
			});
		});
	},

	_openDocument: function( modelName, objectId ) {
		this.setState({
			documentOpen: true,
			currentDocument: {
				modelName: modelName,
				objectId: objectId
			}
		})
	},

	_closeCurrentDocument: function() {
		this.setState({
			documentOpen: false
		})
	},

	_renderCollectionListItem: function( collectionIndex ) {
		var collectionName = this.state.collections[collectionIndex];
		return (
			<ListItem value={collectionIndex}>{collectionName}</ListItem>
		)
	},

	_logout: function() {
		var element = this;

		// Send request
		fetch('/logout', {
			method: 'POST',
			credentials: 'same-origin',
			headers: new Headers({
				"Content-Type": "application/json"
			})
		}).then(function(response) {
			window.location = '/login';
		});
	},

	_collectionChanged: function(e,index) {
		// reload schema and assign new index
		var newModelName = this.state.collections[index];

		var element = this;
		fetch('/api/' + newModelName, {
			method: 'GET',
			credentials: 'same-origin',
			headers: new Headers({
				"Content-Type": "application/json"
			})
		}).then(function(response) {
			response.json().then(function(json){
				var fields = [];
				for(var key in json.paths) {
					if(key !== '_id' && key !== '__v') {
						fields.push({
							type: json.paths[key].instance,
							name: json.paths[key].path,
							value: undefined
						})
					}
				}

				element.setState({
			    selectedCollectionIndex: index,
					currentSchema: fields,
					searchString: '',
					sortField: ''
				});
			})
		});
	},

	_searchStringChanged: function( event, newSearchString ) {
		this.setState({
			searchString: newSearchString
		})
	},

	_renderSortButton: function( fieldIndex ) {
		var field = this.state.currentSchema[fieldIndex];
		return (
			<FlatButton
				label={ camelToSpace(field.name) }
				style={{ marginLeft: '10px' }}
				onTouchTap={this._handleSortButton.bind(null, field.name)} />
		)
	},

	_handleSortButton: function( fieldName ) {
		this.setState({
			sortField: fieldName
		})
	},

	render: function() {
		return (
			<div>
				<AppBar
						title="Material CMS"
						showMenuIconButton={false}
						style={{ zIndex: '1350' }}
						iconElementRight={function(element){return (
							<div>
								<IconButton tooltip="Logout" onTouchTap={element._logout} iconStyle={{ color: element.state.muiTheme.palette.alternateTextColor }}>
									<FontIcon className="material-icons md-36">exit_to_app</FontIcon>
								</IconButton>
							</div>
						)}(this)} />

				<LeftNav>
					<SelectableList
							style={{
								paddingBottom: '0px',
								paddingTop: '64px'
							}}
							valueLink={{
							    value: this.state.selectedCollectionIndex,
							    requestChange: this._collectionChanged
							}}>
						{/*<ListItem value="-1">All</ListItem>*/}

						{Object
						  .keys(this.state.collections)
						  .map(this._renderCollectionListItem)}
					</SelectableList>
				</LeftNav>

        <FloatingActionButton
						ref="newDocumentButton"
						className="new-document-button"
						onTouchTap={this._createNewCurrentDocument}
						style={{
							zIndex: '10'
						}}>
					<FontIcon className="material-icons md-36">add</FontIcon>
				</FloatingActionButton>


				<div className="workspace" ref="workspace">
					<div className="search-bar">
						<FontIcon className="material-icons md-36">search</FontIcon>
						<TextField onChange={this._searchStringChanged} />
						{Object
			              .keys(this.state.currentSchema)
			              .map(this._renderSortButton)}
					</div>

					<div style={{ marginLeft: '5px' }}>
						<DocumentCollection
							modelName={this.state.collections[this.state.selectedCollectionIndex]}
							searchString={this.state.searchString}
							sortField={this.state.sortField}
							openDocument={this._openDocument} />
					</div>

					<Document
						open={this.state.documentOpen}
						title={'Editing ' + this.state.currentDocument.modelName}
						modelName={this.state.currentDocument.modelName}
						objectId={this.state.currentDocument.objectId}
						closeDocument={this._closeCurrentDocument} />
				</div>
			</div>
		)
	}
})

module.exports = App;
