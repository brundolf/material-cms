
import React from 'react';
import ReactDOM from 'react-dom';

import FloatingActionButton from 'material-ui/lib/floating-action-button';
import FontIcon from 'material-ui/lib/font-icon';
import Popover from 'material-ui/lib/popover/popover';
import Menu from 'material-ui/lib/menus/menu';
import MenuItem from 'material-ui/lib/menus/menu-item';
import Divider from 'material-ui/lib/divider';


var NewDocumentMenu = React.createClass({

	getInitialState: function() {
		return {
			open: false
		};
	},

	_openMenu: function(event) {
		this.setState({
	      open: true,
	      anchorEl: event.currentTarget,
	    });

		event.preventDefault();
	},

	_closeMenu: function() {
		this.setState({
		  open: false,
		});
	},

	_selectOption: function(option) {
		this._closeMenu();
		this.props.newDocumentFunction(option);
	},

	_renderCollectionOption: function(collectionIndex){
		var collectionName = this.props.collections[collectionIndex];
		return (
		  <MenuItem
		  	className="new-document-option"
			modelName="instrument"
			onTouchTap={this._selectOption.bind(null, collectionName)}>{'New ' + collectionName}</MenuItem>
		)
	},

	render: function() {
		return (
			<div>
				<FloatingActionButton
						ref="newDocumentButton"
						className="new-document-button"
						onTouchTap={this._openMenu}
						style={{
							zIndex: '10'
						}}>
					<FontIcon className="material-icons md-36">add</FontIcon>
				</FloatingActionButton>

				<Popover ref="newDocumentMenu"
						className="new-document-menu"
						open={this.state.open}
        				anchorEl={this.state.anchorEl}
						useLayerForClickAway={true}
						onRequestClose={this._closeMenu}
						anchorOrigin={{"horizontal":"right","vertical":"top"}}
						targetOrigin={{"horizontal":"right","vertical":"bottom"}}
						style={{
							zIndex: '20'
						}}>
					<Menu>
						{Object
						  .keys(this.props.collections)
						  .map(this._renderCollectionOption)}
						<Divider />
						<MenuItem className="new-document-option">New Document</MenuItem>
					</Menu>
				</Popover>
			</div>
		)
	}

});

module.exports = NewDocumentMenu;
