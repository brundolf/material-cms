
import React from 'react';
import ReactDOM from 'react-dom';

import Paper from 'material-ui/lib/paper';
import IconButton from 'material-ui/lib/icon-button';
import FontIcon from 'material-ui/lib/font-icon';
import TouchRipple from 'material-ui/lib/ripples/touch-ripple';
import getMuiTheme from 'material-ui/lib/styles/getMuiTheme';

function camelToSpace( string ) {
	var spaced = string.replace( /([A-Z])/g, " $1" );
	return spaced.charAt(0).toUpperCase() + spaced.slice(1);
}

var DocumentCard = React.createClass({

	getInitialState: function() {
		return {
			muiTheme: getMuiTheme()
		};
	},

	_openDocument: function(event) {
    if(!event.target.classList.contains('card-delete-button') && !event.target.classList.contains('card-delete-button-icon')) {
  		this.props.openDocument(this.props.document.modelName, this.props.document.objectId);
    }
	},

	_deleteDocument: function() {
		var element = this;
		fetch('/api/' + this.props.document.modelName + '/' + this.props.document.objectId, {
			method: 'DELETE',
			credentials: 'same-origin',
			headers: new Headers({
				"Content-Type": "application/json"
			})
		}).then(function(response) {
			element.props.dataChanged();
		});

		event.stopPropagation();
	},

	render: function( ) {
		return (
			<Paper
					zDepth={2}
					className="document-card"
					onClick={this._openDocument} >
				<TouchRipple
					muiTheme={this.state.muiTheme} >

					{function(element){
						var fields = [];
						for(var i = 0; i < element.props.document.fields.length; i++) {
							fields.push(
							<div style={{ padding: '5px 0px', borderBottom: 'lightgray 1px solid' }}>
								<span style={{ fontWeight:'bold' }}>{camelToSpace(element.props.document.fields[i].name) + ': '}</span>
								<span>{element.props.document.fields[i].value}</span>
							</div>
							)
						}
						return fields;
					}(this)}

					<IconButton
							className="card-delete-button"
							tooltip="Delete"
							tooltipPosition="top-center"
							style={{
								position: 'absolute',
								right: '10px',
								bottom: '10px'
							}}
							onClick={this._deleteDocument}>
						<FontIcon className="card-delete-button-icon material-icons md-36">delete</FontIcon>
					</IconButton>
				</TouchRipple>
			</Paper>
		)
	}
});

module.exports = DocumentCard;
