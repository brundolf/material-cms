
import React from 'react';
import ReactDOM from 'react-dom';

import Paper from 'material-ui/lib/paper';
import IconButton from 'material-ui/lib/icon-button';
import FontIcon from 'material-ui/lib/font-icon';
import TouchRipple from 'material-ui/lib/ripples/touch-ripple';
import getMuiTheme from 'material-ui/lib/styles/getMuiTheme';

import DocumentCard from './DocumentCard.js';


function camelToSpace( string ) {
	var spaced = string.replace( /([A-Z])/g, " $1" );
	return spaced.charAt(0).toUpperCase() + spaced.slice(1);
}

var DocumentCollection = React.createClass({

	getInitialState: function() {
		return {
			muiTheme: getMuiTheme(),
			expanded: false,

			documents: []
		};
	},

	componentWillReceiveProps: function (nextProps) {
		this._loadDocuments(nextProps);
	},

	_loadDocuments: function(nextProps) {
		var props = this.props
		if(typeof nextProps !== 'undefined') {
			props = nextProps;
		}

		var queryParams = [];
		if(props.searchString) {
			queryParams.push('searchString=' + props.searchString);
		}
		if(props.modelName) {
			queryParams.push('modelName=' + props.modelName);
		}
		if(props.sortField) {
			queryParams.push('sortField=' + props.sortField);
		}
		if(props.sortDirection) {
			queryParams.push('sortDirection=' + props.sortDirection);
		}

		var element = this;
		fetch('/api/search?' + queryParams.join('&'),
				{
			method: 'GET',
			credentials: 'same-origin',
			headers: new Headers({
				"Content-Type": "application/json"
			})
		}).then(function(response) {
			response.json().then(function(json){
				var documents = [];
				for(var index in json) {
					var fields = [];
					for(var key in json[index]) {
						if(key !== '_id' && key !== '__v') {
							fields.push({
								name: key,
								value: json[index][key]
							})
						}
					}
					documents.push({
						modelName: props.modelName,
						objectId: json[index]['_id'],
						fields: fields
					})
				}
				element.setState({ documents: documents });
			})
		});
	},

	_deleteDocument: function( documentIndex, event ) {
		var element = this;
		fetch('/api/' + this.state.documents[documentIndex].modelName + '/' + this.state.documents[documentIndex].objectId, {
			method: 'DELETE',
			credentials: 'same-origin',
			headers: new Headers({
				"Content-Type": "application/json"
			})
		}).then(function(response) {
			element._loadDocuments();
		});

		event.stopPropagation();
	},

	_blah: function() {
		console.log('blah')
	},

	_renderDocument: function( documentIndex ) {
		var document = this.state.documents[documentIndex];
		//				{this.props.openDocument.bind(null, document.modelName, document.objectId)}

		return (
			<DocumentCard
				document={document}
				dataChanged={this._loadDocuments}
				openDocument={this.props.openDocument}
				key={'document-' + document.modelName + '-' + document.objectId} />
		)
	},

	render: function() {
		return (
			<div className="document-collection">
				{Object
	              .keys(this.state.documents)
	              .map(this._renderDocument)}
			</div>
		)
	}

});

module.exports = DocumentCollection;
