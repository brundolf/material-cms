
// express
var express = require('express');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var cookieParser = require('cookie-parser');
var session = require('express-session');

var requireDir = require('require-dir');
var path = require('path');

// start
module.exports = {
  init: function(dbUrl, schemas, port) {

    // application
    var app = express();
    var oneDay = 8.64e+7;
    app.use(express.static( __dirname + '/public', { maxAge: oneDay }));

    // Use parameters
    app.set('dbpath', dbUrl);
    mongooseConnections = {};
    mongooseSchemas = (schemas || {});
    generateModel = function(schemaName, connection) {
      return connection.model('Project', mongooseSchemas[schemaName]);
    };
    port = (port || 8001);

    // body parser
    app.use(bodyParser.json());

    // sessions
    app.use(cookieParser('foo'));
    app.use(session({
      secret: 'foo',
      resave: false,
      saveUninitialized: true,
      cookie: {
    	  secure: false,
    	  httpOnly: false
      }
    }));

    // load routes
    var routes = requireDir(__dirname + '/rest-routes');

    // routes
    app.get('/', function(req, res) {
    	if(mongooseConnections[req.session.id]) {
    		res.sendFile(__dirname + '/views/index.html');
    	} else {
    		res.redirect('/login');
    	}
    });
    app.get('/login', function(req, res) {
    	res.sendFile(__dirname + '/views/login.html');
    });
    app.post('/login', function(req, res) {
    	var newConnection = mongoose.createConnection(app.get('dbpath'), {
    			user: req.body.username,
    			pass: req.body.password
    		},
    		function(err) {
    			if(err) {
    				res.send('{"message": "error"}');
    			}
    		}
    	);

    	newConnection.once('connected', function(){
    		mongooseConnections[req.sessionID] = {
    			connection: newConnection,
    			models: {}
    		};
    		res.send('{"message": "success"}');
    	});
    });
    app.post('/logout', function(req, res) {
    	delete mongooseConnections[req.sessionID];
    	res.send();
    });

    // data routes
    app.get('/favicon.ico', function(req, res){ res.status(204).send(); } );
    app.get('/api/collections', routes.collections.allCollections);
    app.get('/api/search', routes.collections.search);

    app.post('/api/:modelName', routes.crud.create);
    app.get('/api/:modelName/:objectId', routes.crud.read);
    app.put('/api/:modelName/:objectId', routes.crud.update);
    app.delete('/api/:modelName/:objectId', routes.crud.delete);
    app.get('/api/:modelName', routes.crud.readSchema);

    // process
    process.on('SIGINT', function() {
      mongoose.connection.close(function () {
        console.log('Mongoose default connection disconnected through app termination');
        process.exit(0);
      });
    });

    app.listen(port);
    console.log('Material CMS listening on port ' + port + '...');
    if(typeof schemas === 'undefined') {
      console.warn('WARNING: No mongoose schemas passed to Material CMS');
    }

  }
};
